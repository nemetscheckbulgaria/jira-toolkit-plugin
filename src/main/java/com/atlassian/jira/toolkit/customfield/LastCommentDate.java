package com.atlassian.jira.toolkit.customfield;

import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.DateField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.toolkit.customfield.format.LastCommentDateFormatter;

import javax.annotation.Nonnull;

/**
 * @since v0.26
 */
public class LastCommentDate extends CalculatedCFType<Date, Date> implements DateField
{
    private final CommentManager commentManager;

    public LastCommentDate(CommentManager commentManager)
    {
        this.commentManager = commentManager;
    }

    @Override
    public String getStringFromSingularObject(Date singularObject)
    {
        if (singularObject == null)
        {
            return null;
        }
        else
        {
            return String.valueOf(singularObject.getTime());
        }
    }

    @Override
    public Date getSingularObjectFromString(String string) throws FieldValidationException
    {
        if (string == null)
        {
            return null;
        }
        else
        {
            return new Date(Long.valueOf(string));
        }
    }

    @Override
    public Date getValueFromIssue(CustomField field, Issue issue)
    {
        List<Comment> comments = commentManager.getComments(issue);
        if (comments == null)
        {
            return issue.getCreated();
        }

        final ListIterator<Comment> it = comments.listIterator();
        // move to the end
        while (it.hasNext())
        {
            it.next();
        }
        // now iterate backwards
        while (it.hasPrevious())
        {
            final Comment comment = it.previous();
            if (isPublicComment(comment))
            {
                return comment.getCreated();
            }
        }
        // No public comments - return the issue creation date
        return issue.getCreated();
    }

    private boolean isPublicComment(Comment comment)
    {
        return comment.getGroupLevel() == null && comment.getRoleLevelId() == null;
    }

    @Nonnull
    public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field,
            final FieldLayoutItem fieldLayoutItem)
    {
        Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);
        map.put("formatter", new LastCommentDateFormatter());
        return map;
    }
}
