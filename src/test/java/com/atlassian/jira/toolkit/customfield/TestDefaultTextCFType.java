/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;

import junit.framework.TestCase;
import com.mockobjects.dynamic.Mock;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.config.CoreFeatures.ON_DEMAND;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultTextCFType extends TestCase
{
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    private Mock customFieldValuePersister;
    private Mock genericConfigManager;
    private Mock featureManager;
    private Mock rendererManager;

    @Before
    public void setUp() throws Exception
    {
        super.setUp();
        customFieldValuePersister = new Mock(CustomFieldValuePersister.class);
        genericConfigManager = new Mock(GenericConfigManager.class);
        featureManager = new Mock(FeatureManager.class);
        rendererManager = new Mock(RendererManager.class);
    }

    private void commonAssertions(String value, String expecting)
    {
        final Map<String, Object> params = new HashMap<String, Object>();
        DefaultTextCFType defaultTextCFType = new DefaultTextCFType( (CustomFieldValuePersister) customFieldValuePersister.proxy(), (GenericConfigManager) genericConfigManager.proxy(), (FeatureManager) featureManager.proxy(), (RendererManager) rendererManager.proxy());
        defaultTextCFType.setVelocityValueParametersValue(params, value);
        assertEquals(expecting, params.get("value"));
        assertEquals(expecting, params.get("defaultValue"));
    }

    @Test
    public void testOnDemandWikiMarkup()
    {
        setUpOnDemandFeature(true);
        String value = "somevalue";
        /* as the provided RendererManager is a mock and has no configured return value for the getRenderedContent call assert the the set value is empty */
        String expecting = "";
        commonAssertions(value, expecting);
    }

    @Test
    public void testNonOnDemandHtmlMarkup()
    {
        setUpOnDemandFeature(false);
        String value = "<image src='http://example.com/image.jpg'>";
        String expecting = value;
        commonAssertions(value, expecting);
    }

    private void setUpOnDemandFeature(boolean onDemand)
    {
        featureManager.expectAndReturn("isEnabled", ON_DEMAND, onDemand);
    }

    @After
    public void tearDown() throws Exception
    {
        super.tearDown();
        JiraAuthenticationContextImpl.getRequestCache().clear();
    }

}
